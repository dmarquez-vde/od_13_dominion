# -*- coding: utf-8 -*-
from openerp import fields,models,api,_
from datetime import date
import openerp.addons.decimal_precision as dp


class AccountMove(models.Model):

    _inherit='account.move'

    sap_code = fields.Char(string="Código SAP")