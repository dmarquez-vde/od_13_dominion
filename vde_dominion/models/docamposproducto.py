from odoo import api, fields, models, _

class ProductTemplate(models.Model):
    _inherit = "product.template"
    #informacion Producto------------------------------------------------------------
    code_sap = fields.Char(string="Código Sap")
    #Detalles de la descripcion------------------------------------------------------
    responsable = fields.Char(string="Responsable")

