# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class CrmLead(models.Model):
    _inherit = "crm.lead"
    #Informacion del prospecto ---------------------------------------------------

    sucursal = fields.Char(string="Sucursal")
    origin_pro = fields.Char(string="Origen de Prospecto")
    origin_espe = fields.Char(string="Origen Especifico")
    specific = fields.Char(string="Especifique")
    inscripcion_rfc = fields.Char(string="Inscripcíon al RFC")
    state_prospec = fields.Char(string="Estado del Prospecto")
    type_move = fields.Char(string="Tipo de Tramite")
    
    #Datos generales -------------------------------------------------------------
    razon_social = fields.Char(string="Razón Social")
    rfc = fields.Char(string="RFC")
    type_client = fields.Char(string="Tipo de Cliente")
    phone_two = fields.Char(string="Telefono 2")
    id_sap = fields.Char(string="ID SAP")
    mail_fac = fields.Char(string="Correo Envió Factura")
    #Detalle de la descripcion--------------------------------------------------------
    forma_pago = fields.Char(string="Forma de pago ")
    account_bank = fields.Char(string="Cuena Bancaria")
    metodo_pago = fields.Char(string="Metodo de pago ")
    payment_conditions = fields.Char(string="Condiciones de pago")
    uso_cfdi = fields.Char(string="Uso de CFDI")

#infomracion lista prospecto ---------------------------------------------------

#Lista sucursal -----------------------------------------------------------------
class sucursallist(models.Model):
    _name = "sucursal.list"

    name = fields.Char(string="Nombre Sucursal")

#Lista prospecto --------------------------------------------------------------------
class origenlist(models.Model):
    _name = "origenprospecto.list"

    name = fields.Char(string="Origen de Prospecto")

#Lista Grupo ------------------------------------------------------------------------
class grupolist(models.Model):
    _name = "grupo.list"

    name = fields.Char(string="Grupo")

#Lista Origen espesifico ------------------------------------------------------------------------
class origenespelist(models.Model):
    _name = "origenespe.list"

    name = fields.Char(string="Origen ")

#Lista Tipo de tramite  ------------------------------------------------------------------------
class Tipotramite(models.Model):
    _name = "tipotramite.list"

    name = fields.Char(string="Tipo de Tramite ")
#campos usuario --------------------------------------------------------------------------------

class Resuser(models.Model):
    _inherit = "res.users"

    code_sap = fields.Char(string="Codigo Sap")


