# -*- coding: utf-8 -*-
from openerp import fields,models,api,_
from datetime import date
import openerp.addons.decimal_precision as dp


class c_formapago (models.Model):

    _name='c.formapago'

    code = fields.Char(string='Forma Pago',  required=True)
    name = fields.Char(string='Descripcion',  required=True)
    num_operacion = fields.Char(string='Número operación', required=True)
    rfc_ordenante = fields.Char(string='RFC Emisor Cuenta Ordenante',  required=True)
    cuentao = fields.Char(string='Cuenta Ordenante',  required=True)
    patronc = fields.Char(string='Patrón cuenta ordenante')
    rfc_beneficiario = fields.Char(string='RFC Emisor Cuenta Beneficiario',  required=True)
    cuentab = fields.Char(string='Cuenta Benenficiario',  required=True)
    patroncb = fields.Char(string='Patrón cuenta Beneficiaria')
    tipo_pago = fields.Char(string='Tipo Cadena Pago',  required=True)
    name_banco = fields.Char(string='Nombre Banco Emisor Cuenta Ordenante extranjero',  required=True)
