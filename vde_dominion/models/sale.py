# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, RedirectWarning, ValidationError
import requests
import json


class SaleOrder(models.Model):
    _inherit = "sale.order"

    estado_envi = fields.Char(string="Estado de Envío")
    sap_quote_log = fields.Text(string="SAP Log Quote", copy=False)
    sap_quote_docEntry = fields.Char(string="SAP DocEntry Quote", copy=False)
    sap_quote_docNum = fields.Char(string="SAP DocNum Quote", copy=False)
    sap_so_log = fields.Text(string="SAP Log SO", copy=False)
    sap_so_docEntry = fields.Char(string="SAP DocEntry SO", copy=False)
    sap_so_docNum = fields.Char(string="SAP DocNum SO", copy=False)
    sap_quote_sent = fields.Boolean(string="Cotizacion enviada a SAP?", default=False, copy=False)
    sap_so_sent = fields.Boolean(string="Pedido enviado a SAP?", default=False, copy=False)
    po_ids = fields.Many2many('purchase.order','rel_so_to_po','so_id','po_id',string="POs")

    def sap_sent_quote_so(self):
        print ("entrando")
        sap_lines = []
        seq = 0
        for line in self.order_line:
            sap_lines.append(
                {
                    'LineNum': seq,
                    'quantity': line.product_uom_qty,
                    'itemCode': line.product_id.sap_code,
                    'price': line.price_unit,
                    'LineTotal': line.price_subtotal,
                    'Descripcion': line.name,
                    'Multiplo': 7,
                    }
                )
            seq = seq + 1
        if (self.state == 'draft' or self.state=='sent'):
            sap_so = {
                'CardCode': self.partner_id.codigo_sap,
                'Moneda': self.currency_id.name,
                'DocDate': self.date_order.strftime("%d/%m/%Y"),
                'SlpCode': self.user_id.code_sap,
                'OC': self.name,
                'TipoDireccion': 'Entrega',
                'Lineas': sap_lines
                }
        elif (self.state == 'sale'):
            sap_so = {
                'CardCode': self.partner_id.codigo_sap,
                'Moneda': self.currency_id.name,
                'DocDueDate': self.date_order.strftime("%d/%m/%Y"),
                'DocEntry': self.sap_quote_docEntry,
                'DocNum': self.sap_quote_docNum,
                'SlpCode': self.user_id.code_sap,
                'CardName': self.partner_id.name,
                'DocTotal': self.amount_total,
                'TipoDireccion': 'Entrega',
                'Lineas': sap_lines
                }
        print ("sap_so ", sap_so)
        sap_so_json = json.dumps(sap_so)
        print ("sap_so_json ", sap_so_json)
        token_response = self.env.user.company_id.sap_get_token()
        print ("token_response ", token_response)
        if token_response.get('error') == False:
            headers = {
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + str(token_response.get('token')),
                    'Content-Type': 'application/json'
                }
            if (self.state == 'draft' or self.state=='sent'):
                print ("Cotizacion")
                try:
                    response = requests.post(
                        self.env.user.company_id.sap_api_url_quote,
                        #timeout=3.14,
                        headers=headers,
                        data=sap_so_json
                        )
                except:
                    raise UserError(_('Error timeout a: '+ (self.env.user.company_id.sap_api_url_quote)))
                print (("response ", response.json()))
                if response.json().get('error') == True:
                    self.sap_quote_log = response.json().get('message')
                elif response.json().get('error') == False:
                    self.sap_quote_log = response.json().get('message')
                    self.sap_quote_docEntry = response.json().get('DocEntry')
                    self.sap_quote_docNum = response.json().get('DocNum')
                    self.sap_quote_sent = True
            elif (self.state == 'sale'):
                print ("Pedido")
                try:
                    response = requests.post(
                        self.env.user.company_id.sap_api_url_so,
                        #timeout=3.14,
                        headers=headers,
                        data=sap_so_json
                        )
                except:
                    raise UserError(_('Error timeout a: '+ (self.env.user.company_id.sap_api_url_so)))
                print (("response ", response.json()))
                if response.json().get('error') == True:
                    self.sap_so_log = response.json().get('message')
                elif response.json().get('error') == False:
                    self.sap_so_log = response.json().get('message')
                    self.sap_so_docEntry = response.json().get('DocEntry')
                    self.sap_so_docNum = response.json().get('DocNum')
                    self.sap_so_sent = True

