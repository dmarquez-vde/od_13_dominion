# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, RedirectWarning, ValidationError
import requests
import json


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    so_ids = fields.Many2many('sale.order','rel_so_to_po','po_id','so_id',string="SOs")