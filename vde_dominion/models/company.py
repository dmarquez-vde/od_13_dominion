# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, RedirectWarning, ValidationError
import requests

class resCompany(models.Model):

    _inherit = 'res.company'

    sap_api_user = fields.Char(string="Sap user")
    sap_api_pass = fields.Char(string="Sap pass")
    sap_api_url_token = fields.Char(string="Sap url token")
    sap_api_url_stock = fields.Char(string="Sap url stock")
    sap_api_url_quote = fields.Char(string="Sap url quote")
    sap_api_url_so = fields.Char(string="Sap url so")


    def sap_get_token(self):
        api_url = self.env.user.company_id.sap_api_url_token
        params = {"Nombre": self.env.user.company_id.sap_api_user, "Contraseña": self.env.user.company_id.sap_api_pass}
        try:
            response = requests.post(api_url, json=params)
        except:
            raise UserError(_('Error timeout a: '+ (api_url)))
        if response.json().get('error') == False:
            return {'error':False,'token':response.json().get('message'),'message':False}
        else:
            return {'error':True,'token':False, 'message':response.json().get('message')}