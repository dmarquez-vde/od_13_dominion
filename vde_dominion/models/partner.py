# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class ResPartner(models.Model):
    _inherit = "res.partner"
    #informacion cliente------------------------------------------------------------
    codigo_sap = fields.Char(string="Código Sap")
    reason_social = fields.Char(string="Razón social")
    group = fields.Char(string="Grupo")
    rfc = fields.Char(string="RFC")
    phone_two = fields.Char(string="Telefono 2")
    person_contact = fields.Char(string="Persona de Contacto")
    active_client =fields.Boolean(string="Cliente Activo")
    special_client =fields.Boolean(string="Cliente Especial")
    #saldo cliente --------------------------------------------------------------------
    balance_account = fields.Monetary(string="Saldo de cuenta")
    delivery = fields.Monetary(string="Entregas")
    order_client = fields.Monetary(string="Pedido Cliente")
    state_credit = fields.Char(string="Estado de Credito")
    #Condiciones de pago --------------------------------------------------------------
    account_bank = fields.Char(string="Cuenta Bancaria")
    metodo_pago = fields.Char(string="Metodo de Pago")
    payment_conditions = fields.Char(string="Condiciones de pago")
    uso_cfdi = fields.Char(string="uso de CFDI")
    limit_credit = fields.Char(string="Limite de Crédito")
    allow_delivery =fields.Boolean(string="Permitir entrega parcial del pedido")
    category_id = fields.Many2many('crm.lead.tag', string="Categoria")

























