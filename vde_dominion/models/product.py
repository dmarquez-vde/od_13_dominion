# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import requests

class productTemplate(models.Model):

    _inherit = 'product.template'

    sap_code = fields.Char(string="Código SAP")
    responsable = fields.Char(string="Responsable")
    sap_stock = fields.Float(string="Stock SAP")
    sap_stock_pedido = fields.Float(string="Stock SAP Pedido")
    sap_stock_comprometido = fields.Float(string="Stock SAP Comprometido")
    sap_stock_disponible = fields.Float(string="Stock SAP Disponible")
    sap_alm_1 = fields.Float(string="ALAM-12")
    sap_alm_2 = fields.Float(string="ALAM-LEN")
    sap_alm_3 = fields.Float(string="CAN-11")
    sap_alm_4 = fields.Float(string="CDMX")
    sap_alm_5 = fields.Float(string="GARANTIA")
    sap_alm_6 = fields.Float(string="GDL-10")
    sap_alm_7 = fields.Float(string="IMP")
    sap_alm_8 = fields.Float(string="LAB-09")
    sap_alm_9 = fields.Float(string="LENTOMOV")
    sap_alm_10 = fields.Float(string="LEO-CONS")
    sap_alm_11 = fields.Float(string="LEON-08")
    sap_alm_12 = fields.Float(string="MEX-01")
    sap_alm_13 = fields.Float(string="MEX-CONS")
    sap_alm_14 = fields.Float(string="MTY-06")
    sap_alm_15 = fields.Float(string="MTY-CONS")
    sap_alm_16 = fields.Float(string="OUTLET")
    sap_alm_17 = fields.Float(string="PUE-04")
    sap_alm_18 = fields.Float(string="PUE-CONS")
    sap_alm_19 = fields.Float(string="QRO-03")
    sap_alm_20 = fields.Float(string="QRO-CONS")
    sap_alm_21 = fields.Float(string="REPRO")
    sap_alm_22 = fields.Float(string="REV-02")
    sap_alm_23 = fields.Float(string="REV-CONS")
    sap_alm_24 = fields.Float(string="SHOWROOM")
    sap_alm_25 = fields.Float(string="TRANS")
    sap_alm_26 = fields.Float(string="VIC-05")
    sap_alm_27 = fields.Float(string="VIC-CONS")
    moneda_base = fields.Many2one('res.currency', string="Moneda Base")
    articulo_stock = fields.Boolean(string="Articulo Stock")
    sap_active = fields.Boolean(string="Articulo Activo")
    sap_proveedor = fields.Char(string="Nombre Proveedor")

    def sap_get_stock(self):
        almacenes_SAP = {
            'ALAM-12':'sap_alm_1',
            'ALAM-LEN':'sap_alm_2',
            'CAN-11':'sap_alm_3',
            'CDMX':'sap_alm_4',
            'GARANTIA':'sap_alm_5',
            'GDL-10':'sap_alm_6',
            'IMP':'sap_alm_7',
            'LAB-09':'sap_alm_8',
            'LENTOMOV':'sap_alm_9',
            'LEO-CONS':'sap_alm_10',
            'LEON-08':'sap_alm_11',
            'MEX-01':'sap_alm_12',
            'MEX-CONS':'sap_alm_13',
            'MTY-06':'sap_alm_14',
            'MTY-CONS':'sap_alm_15',
            'OUTLET':'sap_alm_16',
            'PUE-04':'sap_alm_17',
            'PUE-CONS':'sap_alm_18',
            'QRO-03':'sap_alm_19',
            'QRO-CONS':'sap_alm_20',
            'REPRO':'sap_alm_21',
            'REV-02':'sap_alm_22',
            'REV-CONS':'sap_alm_23',
            'SHOWROOM':'sap_alm_24',
            'TRANS':'sap_alm_25',
            'VIC-05':'sap_alm_26',
            'VIC-CONS':'sap_alm_27',
            }
        token_response = self.env.user.company_id.sap_get_token()
        print ("token_response ", token_response)
        for record in self:
            if token_response.get('error') == False:
                headers = {
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + str(token_response.get('token')),
                    'Content-Type': 'application/json'
                }
                producto = record.sap_code
                almacen = ''
                api_url_stock = self.env.user.company_id.sap_api_url_stock + '?ItemCode='+str(producto)+'&Almacen='+str(almacen)
                stock_response = requests.get(api_url_stock, timeout=3.14, headers=headers)
                stock = 0
                almacenes = {}
                for key in stock_response.json():
                    almacen = key.get('WhsCode')
                    almacenes.update({almacenes_SAP.get(almacen): key.get('Onhand')})
                    stock = stock + float(key.get('Onhand'))
                record.sap_stock = stock
                record.sap_alm_1 = almacenes.get('sap_alm_1')
                record.sap_alm_2 = almacenes.get('sap_alm_2')
                record.sap_alm_3 = almacenes.get('sap_alm_3')
                record.sap_alm_4 = almacenes.get('sap_alm_4')
                record.sap_alm_5 = almacenes.get('sap_alm_5')
                record.sap_alm_6 = almacenes.get('sap_alm_6')
                record.sap_alm_7 = almacenes.get('sap_alm_7')
                record.sap_alm_8 = almacenes.get('sap_alm_8')
                record.sap_alm_9 = almacenes.get('sap_alm_9')
                record.sap_alm_10 = almacenes.get('sap_alm_10')
                record.sap_alm_11 = almacenes.get('sap_alm_11')
                record.sap_alm_12 = almacenes.get('sap_alm_12')
                record.sap_alm_13 = almacenes.get('sap_alm_13')
                record.sap_alm_14 = almacenes.get('sap_alm_14')
                record.sap_alm_15 = almacenes.get('sap_alm_15')
                record.sap_alm_16 = almacenes.get('sap_alm_16')
                record.sap_alm_17 = almacenes.get('sap_alm_17')
                record.sap_alm_18 = almacenes.get('sap_alm_18')
                record.sap_alm_19 = almacenes.get('sap_alm_19')
                record.sap_alm_20 = almacenes.get('sap_alm_20')
                record.sap_alm_21 = almacenes.get('sap_alm_21')
                record.sap_alm_22 = almacenes.get('sap_alm_22')
                record.sap_alm_23 = almacenes.get('sap_alm_23')
                record.sap_alm_24 = almacenes.get('sap_alm_24')
                record.sap_alm_25 = almacenes.get('sap_alm_25')
                record.sap_alm_26 = almacenes.get('sap_alm_26')
                record.sap_alm_27 = almacenes.get('sap_alm_27')