# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2010-Present Vos Datos Enterprise Suite SA de CV
#     (<https://vde-suite.com>).
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'VDE Dominion',
    'version': '1.0',
    'depends': [
            'base','crm','sale','purchase','account',
    ],
    'license': 'Other proprietary',
    'price': 9999.0,
    'category': 'Sales',
    'currency': 'EUR',
    'summary': """Dominion custom module""",
    'description': "",
    'author': 'VDE Suite',
    'website': 'https://www.vde-suite.com',
    'support': '911@vde-suite.com.com',
    'images': [],

    'data': [
	'security/dominion_groups.xml',
	'security/ir.model.access.csv',
        'views/crm.xml',
        'views/userscrm.xml',
        'views/partner.xml',
        'views/sale_order.xml',
        'views/sucursal_lista.xml',
        'views/grupo_lista.xml',
        'views/origenprospecto_lista.xml',
        'views/origenprosespesifico_lista.xml',
        'views/tipotramite_lista.xml',
        'views/cfdi_lista.xml',
        'views/menus.xml',
        'views/company.xml',
        'views/product.xml',
        'views/invoice.xml',
        'views/purchase_order.xml',
    ],
    'installable': True,
    'application': False,
}


